In Git Bash:

git log --after 01-01-2018 --pretty=format:"%h,%s,%an,%ad" --no-merges --all > <file>.csv

The --all switch grabs all commits from all branches.

