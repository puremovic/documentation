Norman Kramer	Fri, 28 Jul 2017 10:34:24 -0500	Change Version to 4.12.1.0
Norman Kramer	Thu, 27 Jul 2017 11:10:13 -0500	Update Version to 4.11.1.5
Norman Kramer	Thu, 27 Jul 2017 09:09:55 -0500	Utilities for testing Sonus connections
Norman Kramer	Wed, 26 Jul 2017 13:37:39 -0500	Correct settling of the 'Application Id' and 'Destination Queue Id' which was defaulted to 'Diameter' because of 'Cut/Paste' error.
Norman Kramer	Wed, 19 Jul 2017 11:54:02 -0500	Fixes for Sonus processing
Norman Kramer	Wed, 14 Jun 2017 13:25:38 -0500	[TB-80] Yet Another Fix to Serialization
Norman Kramer	Tue, 13 Jun 2017 12:43:16 -0500	[] - New Mnt Version Merge
Lori Redmon	Tue, 13 Jun 2017 10:59:01 -0500	As per Dave's request, modified End-to-End Identifier to hex similar to Hop-by-Hop Identifier.
Norman Kramer	Fri, 9 Jun 2017 13:45:05 -0500	[] - Enhancements to Sonus Processing - provides better handling of unknown input commands.
Norman Kramer	Thu, 8 Jun 2017 10:03:29 -0500	Orri's changes
Norman Kramer	Wed, 7 Jun 2017 15:12:07 -0500	Fix Typo in 'PopulateNdrNumbersDiameter' processing of '6114'
Norman Kramer	Tue, 6 Jun 2017 15:34:57 -0500	[TB-80] - Fix for putting IMEI number into NPR shallow number area.
Norman Kramer	Fri, 2 Jun 2017 13:20:13 -0500	[TB-82] - Correction to M3UA processing
Norman Kramer	Fri, 2 Jun 2017 10:30:39 -0500	In NDR - 'ReportLinkSetName' was including 'garabge' characters.
Norman Kramer	Fri, 2 Jun 2017 10:09:50 -0500	[TB-105] - For Diameter using 'tcap_offset' as 'Request/Answer' indicator.
Lori Redmon	Thu, 1 Jun 2017 12:05:01 -0500	TB-78: Diameter Hop Counters Decimal vs hex.out Modified the Diameter Bits.pbit to change Hop-by-Hop Identifier field for request and answer from Integer to Hex. This then caused the Hop-by-Hop Identifier fields to be displayed as hex.
Norman Kramer	Thu, 1 Jun 2017 11:29:05 -0500	Remove Debug Statement
Norman Kramer	Thu, 1 Jun 2017 10:32:49 -0500	Fix up Dispositions (Diameter), Add Command Code to NDR Msu header
Lori Redmon	Thu, 1 Jun 2017 09:03:09 -0500	TB-4:  Diameter AVP Decode Failure
Norman Kramer	Wed, 31 May 2017 09:05:10 -0500	[TB-70] - Beef up Sonus processing
Norman Kramer	Wed, 31 May 2017 09:04:09 -0500	Fix 'ReportName' issue for Diameter NDRs
Norman Kramer	Tue, 30 May 2017 13:55:32 -0500	Add Sequence Number to Diameter Call Block (mainly for GCT)
Norman Kramer	Tue, 30 May 2017 12:21:47 -0500	[TB-2]Correct Setting of Diameter Disposition
Norman Kramer	Tue, 30 May 2017 11:31:00 -0500	Update version to '4.11.1.3'
Lori Redmon	Tue, 30 May 2017 11:18:31 -0500	Resolving conflict
Lori Redmon	Tue, 30 May 2017 10:50:24 -0500	TB-35: Cluster VLAN Filtration/formatting
Norman Kramer	Tue, 30 May 2017 10:24:48 -0500	[TB-80][TB-51][TB-2]
Lori Redmon	Mon, 22 May 2017 15:51:06 -0500	TB-35:  Cluster VLAN filtration/formatting
Lori Redmon	Fri, 19 May 2017 14:35:00 -0500	TB-4:  Unimplemented AVP
Lori Redmon	Fri, 19 May 2017 10:49:12 -0500	TB-32:  Protocol Engine in ProtocolList.csv enhance "address digits" name
Norman Kramer	Thu, 18 May 2017 14:49:30 -0500	Lori missing fix 'Type_Digits'
Norman Kramer	Thu, 18 May 2017 13:39:09 -0500	Merge of Orri's changes May 16 2017
Norman Kramer	Thu, 18 May 2017 12:01:32 -0500	[tb-4] Adjust Serialization
Norman Kramer	Wed, 17 May 2017 13:42:52 -0500	[] - Turn on Link Table Swap
Lori Redmon	Wed, 17 May 2017 11:28:07 -0500	[TB-79] - Diameter MSISDN Digits in Output and GCT
Norman Kramer	Wed, 17 May 2017 11:17:42 -0500	[TB-2] Realign Diameter Disposition Arrays between the Cluster and Branch Controller
Norman Kramer	Wed, 17 May 2017 10:27:12 -0500	[TB-80] - Fixed problem with the dual use of Diameter AVP 1 - User Name
Norman Kramer	Tue, 16 May 2017 09:40:10 -0500	This is the merge of the Diameter GCT with the current Sonus-Diameter code.
Lori Redmon	Fri, 12 May 2017 10:11:46 -0500	[TB-85] - MSISDN and Request IPV4 Port Local have same Tekno #6226 Value.  Modified code and pbit files so that MSISDN is now numbered 6236.
Lori Redmon	Thu, 11 May 2017 11:44:46 -0500	[TB-38] - more field renumbering for Diameter.
Lori Redmon	Wed, 10 May 2017 10:23:12 -0500	Merging Orry's changes from NetOccupy and VLAN branch to main Diameter-Sonus branch.
Lori Redmon	Tue, 9 May 2017 16:37:34 -0500	[TB-38] - Protocol Engine - create Diameter ProtocolList.csv (for Redshift)
Lori Redmon	Fri, 5 May 2017 08:58:56 -0500	JIRA TB-57 - Modified NDR Defintion - NDR_Tagged_MSU_For_GCT structure, renaming "MsuIsIPPacket" to "StartingBaseProtocol". Also modified cluster code to pass the base protocol the NDR_Consist_Last_Msu_Hex_Dump() and over-ride it when appropriate.
O. Meiri	Wed, 3 May 2017 18:27:42 -0500	Added Diameter GCT and report fixes
Norman Kramer	Tue, 2 May 2017 10:56:05 -0500	[TB-62] - Modifiy usage of Link Table for Sonus so that the 'normal' 'IP' columns can be used as normal.
Lori Redmon	Fri, 28 Apr 2017 10:41:46 -0500	Pulling in setting VLAN and IP fields.  Also corrected the block references.
Norman Kramer	Fri, 28 Apr 2017 10:04:44 -0500	[T-53] - Changes to the populate shallow numbers function to correctlly handle ASCII numbers
Norman Kramer	Fri, 28 Apr 2017 09:15:31 -0500	Allow multiple connections
O. Meiri	Thu, 27 Apr 2017 18:10:08 -0500	Added Other protocols to NetOccupy Added VLAN to NDR
Norman Kramer	Tue, 25 Apr 2017 11:28:42 -0500	[TB-28] Expand allowed value of field 'SLC' from 16 to 255 for Sonus Diameter
Norman Kramer	Mon, 24 Apr 2017 10:00:03 -0500	Update 'Wireshark' encapuslation types
Lori Redmon	Mon, 24 Apr 2017 09:56:14 -0500	Merging XUDT changes from Diameter4.11.1 into Diameter-Sonus-Merge
Norman Kramer	Fri, 21 Apr 2017 09:19:29 -0500	[SRX05372] - Modifications to 'LinkTable' keying to account for including the 'MSU-Direction' indicator from the IMF traffic message header.
Norman Kramer	Thu, 20 Apr 2017 11:03:37 -0500	[SONUS-Memory Exception] Removed 'queue' that was adding data but never removing it.
Norman Kramer	Wed, 19 Apr 2017 13:39:43 -0500	Modifications for SONUS Links
Norman Kramer	Wed, 12 Apr 2017 10:44:15 -0500	Ensure 'LLT_Variable'
Norman Kramer	Wed, 12 Apr 2017 09:53:31 -0500	Sonus Modifications
Norman Kramer	Fri, 31 Mar 2017 11:12:44 -0500	Boost Version for Filter
Norman Kramer	Fri, 31 Mar 2017 10:38:12 -0500	Lori's fix for mixed Ansi/ITU
Norman Kramer	Fri, 31 Mar 2017 10:23:02 -0500	Finish testing for 'EthType2' (ILM)
Norman Kramer	Wed, 29 Mar 2017 10:43:20 -0500	Remove debug statement
Norman Kramer	Wed, 29 Mar 2017 09:47:08 -0500	Further work on adding NDR Opt2/3 data to Diameter. Also print 'V_LAN' information on Dump 7.
O. Meiri	Tue, 28 Mar 2017 15:50:57 -0500	Metrics report fix.
O. Meiri	Tue, 28 Mar 2017 15:26:13 -0500	Fixed msg count report spacing
Lori Redmon	Mon, 27 Mar 2017 14:35:43 -0500	Merging fix for [#SRX05347] ANSI-41 and GSM Message Count Issues into Diameter 4.11.1 branch.
Norman Kramer	Tue, 21 Mar 2017 09:06:39 -0500	Change version to '4.11.1.0'
Norman Kramer	Mon, 20 Mar 2017 10:36:56 -0500	Eliminate various 'warnings'
Norman Kramer	Fri, 17 Mar 2017 15:20:34 -0500	Adjustments for shallow numbers for Diameter
Norman Kramer	Thu, 16 Mar 2017 13:39:05 -0500	Diameter NDR Modifications, Lori's Latest version of 'XUDT' changes.
Lori Redmon	Fri, 10 Mar 2017 12:23:28 -0600	Orry's fix for GCT to print GSM transid as int instead of hex value. Also, in XUDT branch controller code, reset ANSI-41 flag Flag::corrFrwrd that had been toggled during key evaluations. It was reset to original value.
Norman Kramer	Fri, 10 Mar 2017 10:01:52 -0600	Pickup change to 'TcapResponingTID...' translating from BigEndian.
Lori Redmon	Thu, 9 Mar 2017 12:05:01 -0600	Fix to correct disposition received in GCT for ANSI-41 Lone Responses.
Norman Kramer	Tue, 7 Mar 2017 13:31:32 -0600	Diameter Code Clean UP
Lori Redmon	Tue, 7 Mar 2017 10:41:07 -0600	XUDT fixes for: 1) Opening a call block for XUDT messages when no existing match is found.  (Addresses the lone END XUDT MSUs received) 2) Passing the protocol as unspecified for GSM branch controller decodes so that the engine figures the correct protocol. This addresses the -7 error being received when an MSU has a mix of ANSI/ITU (MTP and SCCP are ANSI and TCAP is ITU)
Norman Kramer	Mon, 6 Mar 2017 10:27:20 -0600	Adjust Diameter status counts.
Norman Kramer	Fri, 3 Mar 2017 14:09:14 -0600	Correcting status information to include Diameter - still need to work on queued to cluster
Lori Redmon	Fri, 3 Mar 2017 14:03:53 -0600	XUDT fixes: 1) Set MsuISIPPacket in mini-header to 0 for XUDT segments.  XUDT segments will not be reassembled with IP data so need to set this flag to zero. 2) Set TCAP_Package and TCAP_Component when branch controller returns a NDR block for a "Mangled MSU" when decode fails. 3) Added check so that for IS41 XUDT subsequent MSUs (not initial XUDT MSU) the OpCode is not overwritten. (This maintains original opcode received with initial XUDT MSU).
Norman Kramer	Wed, 1 Mar 2017 14:11:22 -0600	Add in SCTP Protocol 47 for secure Diameter
Norman Kramer	Wed, 1 Mar 2017 13:37:19 -0600	More Intergration between Diameter and the 4.10.x.y branch
Lori Redmon	Thu, 23 Feb 2017 15:56:34 -0600	XUDT fixes:
Norman Kramer	Tue, 21 Feb 2017 13:32:11 -0600	Branch / Cluster merges
Lori Redmon	Fri, 17 Feb 2017 09:43:50 -0600	Correct setting of msu_octets in NDR mini header when reassembly of extended data is done in the cluster. Set version number in NDR mini header for those protocols that were missed.
Norman Kramer	Thu, 16 Feb 2017 11:49:28 -0600	Change for handling 'Tcap...ID' correctly. Change from network to host order. Modified orginal mod to use the 'htohl' system function.
Norman Kramer	Mon, 13 Feb 2017 13:01:32 -0600	Initial pass at Diameter to 4.10 Merge
Lori Redmon	Wed, 8 Feb 2017 10:01:37 -0600	Made the following modifications for SIP: 1) Add setting of delimiter and version number in NDR. 2) Modified SipProc() so that SetWatchListNewEngine() is not called. Originally, SetWatchListNewEngine() only had some debugging lines in it so calling it had no impact.  It has been modified now that ANSI41 is using the new engine so new code will execute that doesn't need to for SIP processing. 3)Removed unneeded lines in SIP processing case of SetEngineFieldWatchesForAllFilters().  The fields being added to the watchlist for SIP did not apply for SIP and the entries that were being added caused breakage of ANS41 processing.
Norman Kramer	Wed, 1 Feb 2017 10:17:13 -0600	In Sip block extensions section made NextAvailByte and BytesLeft 'unsigned'.
Norman Kramer	Tue, 31 Jan 2017 09:29:34 -0600	Fixed Upper Limit on Receive From Media. Include Requested size in error output for exceeding composite call block size to send to Cluster.
Norman Kramer	Fri, 27 Jan 2017 14:23:15 -0600	Fix minor problem with reporting Destination Port in Quick Basic Decode