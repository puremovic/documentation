﻿<?xml version="1.0" encoding="utf-8"?>
<CatapultToc
  Version="1">
  <TocEntry
    Title="NetAnalyzer (m)NAS-500C³(v.2)"
    StartChapter="true"
    PageLayout="/Content/Resources/PageLayouts/FrontMatter.flpgl"
    Link="/Content/Covers/NetAnalyzer_500C³(v2)_Cover.htm"
    PageNumber="1"
    PageNumberReset="reset"
    StartSection="false"
    AutoEndOnLeftPage="disabled"
    PageType="first"
    PageNumberFormat="lower-roman"></TocEntry>
  <TocEntry
    Title="Legal Page"
    Link="/Content/LegalPage.htm"
    PageLayout="/Content/Resources/PageLayouts/FrontMatter.flpgl"
    PageType="left"
    AutoEndOnLeftPage="disabled"
    PageNumber="1"
    PageNumberReset="continue"
    StartSection="false"
    PageNumberFormat="lower-roman"
    BreakType="none"></TocEntry>
  <TocEntry
    Title="TOC"
    Link="/Content/TOC.htm"
    StartChapter="true"
    PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
    PageNumberReset="continue"
    PageNumberFormat="lower-roman"
    PageNumber="1"
    StartSection="false"
    PageType="first" />
  <TocEntry
    Title="Revision History"
    Link="/Content/RevisionHistory.htm"
    StartSection="false" />
  <TocEntry
    Title="Introduction"
    StartChapter="true"
    PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
    PageType="first"
    PageNumberReset="reset"
    PageNumberFormat="decimal"
    PageNumber="1"
    StartSection="true"
    Link="/Content/Introduction/Introduction_to_NetAnalyzer_System.htm">
    <TocEntry
      Title="NetQuest System"
      Link="/Content/NetQuest_About.htm"></TocEntry>
    <TocEntry
      Title="About this Manual"
      Link="/Content/Introduction/NAS-500C³(v2)/About_this_Manual-500C³(v2).htm" />
    <TocEntry
      Title="General Features"
      Link="/Content/Introduction/NAS-500C³(v2)/General_Features_500C³(v2).htm" />
    <TocEntry
      Title="Functional Overview"
      Link="/Content/Introduction/NAS-500C³(v2)/Functional_Overview_500C³(v2).htm"
      StartSection="false"
      StartChapter="false"
      SectionNumberReset="continue"
      VolumeNumberReset="same"
      ChapterNumberReset="continue"
      ComputeToc="false"
      ReplaceMergeNode="false" />
  </TocEntry>
  <TocEntry
    Title="NetAnalyzer (m)NAS-500C³(v.2) Model Descriptions"
    Link="/Content/Introduction/NAS-500C³(v2)/500C³(v2)_Model_Descriptions.htm">
    <TocEntry
      Title="(m)NAS-500C³(v.2)  Product Configurations"
      Link="/Content/Introduction/NAS-500C³(v2)/500C³(v2) Product Configurations.htm" />
  </TocEntry>
  <TocEntry
    Title="Technical Specifications"
    Link="/Content/Introduction/Technical_Specifications.htm">
    <TocEntry
      Title="Technical Specifications (m)NAS-500C³(v.2)"
      Link="/Content/Introduction/NAS-500C³(v2)/Technical_Specifications_500C3(v2).htm"
      StartSection="false"
      conditions="Global_Conditions.Hidden"
      ReplaceMergeNode="Unchecked" />
    <TocEntry
      Title="(m)NAS-500C³(v.2) - Domestic"
      Link="/Content/PDFs/(m)NAS-500C³(v.2)_R2_TS.pdf" />
    <TocEntry
      Title="(m)NAS-500C³(v.2) - International"
      Link="/Content/PDFs/mini NAS-500C3 v2  INTL R2 TS .pdf" />
  </TocEntry>
  <TocEntry
    Title="Card Assemblies"
    Link="/Content/Hardware_Configs/Card_Assemblies.htm"
    BreakType="chapter"
    StartSection="true"
    PageNumberReset="continue"
    PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl">
    <TocEntry
      Title="(m)NAS-500C³(v.2) Card Basket Assemblies"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/500C³(v2)_Card_Bskt_Assbly.htm"
      BreakType="none"
      StartSection="false"
      PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"></TocEntry>
    <TocEntry
      Title="(m)NAS-500C³(v.2) Media and Processor Cards"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/500C3(v2)_Media_Processor_Card_Assemblies.htm"
      StartSection="false" />
  </TocEntry>
  <TocEntry
    Title="Hardware and Wiring Diagrams"
    Link="/Content/Hardware_Configs/NAS-500C³(v.2)/Wiring_Diagrams_C³(v2).htm"
    StartChapter="true"
    PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
    PageNumber="1"
    PageNumberReset="continue"
    StartSection="true"
    AutoEndOnLeftPage="enabled"
    PageType="continue">
    <TocEntry
      Title="96 Link Configuration Hardware Diagrams (4 and 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/96_Link_Config_4_and_2_Slot(v2).htm" />
    <TocEntry
      Title="96 Link Configuration Hardware Diagrams (2x2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/96_Link_Config_2x2_Slot(v2).htm" />
    <TocEntry
      Title="96 Link Configuration Wiring Diagram (4 and 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/96_Link_Wiring_Diag_4_and_2Slot(v2).htm"
      BreakType="chapter"
      StartSection="false"
      PageNumberReset="continue"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl"
      SectionNumberReset="continue"
      VolumeNumberReset="same"
      ChapterNumberReset="continue"
      ComputeToc="false"
      ReplaceMergeNode="false" />
    <TocEntry
      Title="96 Link Configuration Wiring Diagram (1 x 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/96_Link_Wiring_Diag_2-Slot(v2).htm"
      BreakType="pageLayout"
      StartSection="false"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl" />
    <TocEntry
      Title="Running List (m)NAS-500C³(v.2) - 96 Links (4 and 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/Running_List_NAS-500C³(v2)-96_Links_4and2_Slot.htm"
      BreakType="pageLayout"
      StartSection="false"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl" />
    <TocEntry
      Title="Running List (m)NAS-500C³(v.2) - 96 Links (2x2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/Running_List_NAS-500C³(v2)-96_Links_2-Slot.htm"
      BreakType="pageLayout"
      StartSection="false"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl" />
    <TocEntry
      Title="192_Link_Configuration Hardware Diagrams  (4 and 2_Slot_Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/192_Link_Config_4&amp;2_Slot(v2).htm"
      BreakType="chapter"
      PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
      StartSection="false"
      PageNumberReset="continue"
      PageType="continue" />
    <TocEntry
      Title="192 Link Configuration Hardware Diagrams (3x2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/192_link_Config_3x2_Slot(v2).htm"
      StartSection="false" />
    <TocEntry
      Title="192_Link Configuration Wiring Diagram (4 and 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/192_link_Wiring_Diag_4-Slot(v2).htm"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl"
      PageNumber="1"
      PageNumberReset="continue"
      StartSection="false"
      AutoEndOnLeftPage="disabled"
      BreakType="pageLayout"
      PageType="continue"
      SectionNumberReset="continue"
      VolumeNumberReset="same"
      ChapterNumberReset="continue"></TocEntry>
    <TocEntry
      Title="192 Link Configuration Wiring Diagram (3 x 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/192_link_Wiring_Diag_2-Slot(v2).htm"
      BreakType="none"
      StartSection="false"
      PageNumberReset="continue"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl" />
    <TocEntry
      Title="Running List (m)NAS-500C³(v.2) - 192 Links (4 and 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/Running_List_NAS-500C³(v2)-192_Link_4and2-Slot.htm"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl"
      PageNumber="1"
      PageNumberReset="continue"
      StartSection="false"
      PageType="left"
      AutoEndOnLeftPage="disabled"
      BreakType="none" />
    <TocEntry
      Title="Running List (m)NAS-500C³(v.2) - 192 Links (3x2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/Running_List_NAS-500C³(v2)-192_Link_3x2-Slot.htm" />
    <TocEntry
      Title="288 Link Configuration Hardware Diagrams (4 and 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/288_Link_Config_4&amp;2_Slot(v2).htm"
      BreakType="chapter"
      StartSection="false"
      PageNumberReset="continue"
      PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl" />
    <TocEntry
      Title="288 Link Configuration Wiring Diagram (4 and 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/288_link_Wiring_Diag_4and2-Slot(v2).htm"
      BreakType="pageLayout"
      StartSection="false"
      PageNumberReset="continue"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl"
      PageType="continue" />
    <TocEntry
      Title="Running List (m)NAS-500C³(v.2) 288 Links (4 and 2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/Running_List_NAS-500C³(v2)-288_Link_4and2-Slot.htm"
      StartSection="false" />
    <TocEntry
      Title="864 Link Configuration Hardware Diagrams 2 + 4 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/864_Link_Config(v2).htm"
      BreakType="chapter"
      PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
      StartSection="false"
      PageNumberReset="continue"
      PageType="continue" />
    <TocEntry
      Title="864 Link Configuration Wiring Diagram (3x2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/864_Link_Wiring_Diag_2-Slot(v2).htm"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl"
      PageNumber="1"
      PageNumberReset="continue"
      StartSection="false"
      AutoEndOnLeftPage="disabled"
      PageType="right"
      BreakType="pageLayout" />
    <TocEntry
      Title="Running List (m)NAS-500C³ 864 Links (2 Slot Backplanes)"
      Link="/Content/Hardware_Configs/NAS-500C³(v.2)/Running_List_NAS-500C³-864_Links_2-Slot(v2).htm"
      PageLayout="/Content/Resources/PageLayouts/Chapter_landscape.flpgl"
      PageNumber="1"
      PageNumberReset="continue"
      StartSection="false"
      PageType="left"
      AutoEndOnLeftPage="disabled"
      BreakType="none" />
  </TocEntry>
  <TocEntry
    Title="Installation Procedures"
    Link="/Content/Installation/Installation_Procedures_NAS.htm"
    StartChapter="true"
    PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
    PageNumber="1"
    PageNumberReset="continue"
    StartSection="true"
    PageType="continue">
    <TocEntry
      Title="Installation Summary"
      Link="/Content/Installation/NAS-500C³(v.2)/Installation_SummaryC³(v2).htm"
      StartSection="false" />
    <TocEntry
      Title="Installation Preparation"
      Link="/Content/Installation/2k-1k-500_Install/Phase_2-Installation_Preparation.htm">
      <TocEntry
        Title="Planning and Site Preparation"
        Link="/Content/Installation/NAS-500C³(v.2)/Planning_Site_Preparation-500C3(v2).htm"></TocEntry>
      <TocEntry
        Title="Tools and Supplies"
        Link="/Content/Installation/2k-1k-500_Install/Tools_and_Supplies.htm" />
      <TocEntry
        Title="Unpacking Instructions"
        Link="/Content/Installation/NAS-500C³/Unpacking_Instructions_500C³.htm" />
    </TocEntry>
    <TocEntry
      Title="Perform Installation"
      Link="/Content/Installation/NAS-500C³(v.2)/Perform_Installation_NAS500C3(v2).htm">
      <TocEntry
        Title="Connect Power"
        Link="/Content/Installation/NAS-500C³(v.2)/Connect_Power_NAS-500C³(v2).htm" />
      <TocEntry
        Title="Install Keyboard, Video, Mouse (KVM) Switch"
        Link="/Content/Installation/NAS-500C³(v.2)/Install_KVM_Switch_C³(v2).htm"
        conditions="Install_Hardware.Optional KVM" />
      <TocEntry
        Title="Terminate SS7 Links to Isolation Panel"
        Link="/Content/Installation/NAS-500C³/Terminate SS7 Links to Isolation Panel.htm" />
      <TocEntry
        Title="Verify SS7 Links Online"
        Link="/Content/Installation/Verify_SS7_Links_Online.htm" />
      <TocEntry
        Title="Media Status"
        Link="/Content/MNT/Media_Status/Media_Status.htm"
        BreakType="chapter"
        PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
        AutoEndOnLeftPage="disabled"
        StartSection="false"
        PageNumberReset="continue"
        PageType="continue">
        <TocEntry
          Title="Media Status - Introduction"
          Link="/Content/MNT/Media_Status/Media_Status_Introduction.htm" />
        <TocEntry
          Title="Media Status - Overview"
          Link="/Content/MNT/Media_Status/Media_Status_Overview.htm"
          BreakType="none"
          PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
          AutoEndOnLeftPage="disabled"
          StartSection="false"
          PageNumberReset="continue" />
        <TocEntry
          Title="Media Status - Screen Sample"
          Link="/Content/MNT/Media_Status/Media_Status_Screen_Sample.htm"
          BreakType="none"
          StartSection="false"
          PageNumberReset="continue"
          AutoEndOnLeftPage="disabled" />
      </TocEntry>
    </TocEntry>
  </TocEntry>
  <TocEntry
    Title="Maintenance Procedures"
    Link="/Content/Maintenance/Maintenance_Procedures.htm"
    PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
    PageNumber="1"
    PageNumberReset="continue"
    StartSection="true"
    AutoEndOnLeftPage="disabled"
    BreakType="chapter"
    PageType="continue">
    <TocEntry
      Title="Maintenance of (m)NAS-500C³(v.2)"
      Link="/Content/Maintenance/Model_B_Chassis/Maintenance_of_Model_B_Chassis.htm"
      BreakType="none"
      StartSection="false"
      PageNumberReset="continue"
      PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl" />
  </TocEntry>
  <TocEntry
    Title="Contact Tekno Telecom"
    Link="/Content/Contact_Tekno.htm"
    PageLayout="/Content/Resources/PageLayouts/Chapters.flpgl"
    PageNumber="1"
    PageNumberReset="continue"
    StartSection="true"
    BreakType="chapter"
    AutoEndOnLeftPage="enabled"
    PageType="right" />
  <TocEntry
    Title="Glossary"
    Link="/Content/Glossary.htm"
    StartChapter="true"
    PageLayout="/Content/Resources/PageLayouts/Glossary.flpgl"
    PageNumber="1"
    PageNumberReset="continue"
    StartSection="true"
    AutoEndOnLeftPage="enabled"
    PageType="right" />
  <TocEntry
    Title="Index"
    Link="/Content/Index.htm"
    BreakType="chapter"
    PageLayout="/Content/Resources/PageLayouts/Index.flpgl"
    StartSection="true"
    PageNumberReset="continue"
    PageType="right" />
</CatapultToc>