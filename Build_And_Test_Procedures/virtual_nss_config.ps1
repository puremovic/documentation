#########################################
#                                       #
#   Configure Virtual NetStorage Server #
#   Copy/paste into PowerShell          #
#                                       #
#########################################

# Locally on �NAS-100C�(v2) BOX (PROBE):
# Enable Hyper-V 
Get-WindowsFeature Hyper-V* <# This command checks current Hyper-V status. #>
Install-WindowsFeature Hyper-V-Tools 

# Locally on the NSS virtual machine:
# (1) Enable RDP connections.
Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Control\Terminal Server" -Name "fDenyTSConnections" �Value 0

# (2) Configure Windows Firewall to allow RDP connections.
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"

# (3) Enable IIS 6 compatibility.
Import-Module servermanager
Add-WindowsFeature Web-WMI,Web-Scripting-Tools