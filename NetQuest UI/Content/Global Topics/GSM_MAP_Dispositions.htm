﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../CONCEPTS/Resources/Stylesheets/BookStyles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h2>GSM MAP Dispositions</h2>
        <p>
            <MadCap:glossaryTerm>GSM</MadCap:glossaryTerm> <MadCap:glossaryTerm>MAP</MadCap:glossaryTerm> dispositions are divided into categories that indicate the degree of successful correlation or the layer of the <MadCap:glossaryTerm>SS7</MadCap:glossaryTerm> protocol that created an error disposition. </p>
        <ul>
            <li class="bulletitem"><a href="#GSM_Correlation" class="for_print">GSM Correlation</a>
            </li>
            <li class="bulletitem">GSM Transaction Aborts</li>
            <li class="bulletitem">GSM User Errors</li>
            <li class="bulletitem">GSM Other</li>
        </ul>
        <h3><a name="GSM_Correlation"></a>GSM Correlation Scenarios</h3>
        <p>The following scenarios illustrate the GSM correlation scheme and associated dispositions that result from them. GSM <MadCap:glossaryTerm>MSUs</MadCap:glossaryTerm> are assembled into a transaction block based on a unique key value built from fields that identify MSUs belonging to a particular GSM transaction. Up to ten GSM MSUs (01 – 10) may be correlated together into one transaction block. Typically, GSM MSUs are correlated in pairs as provided by <MadCap:glossaryTerm>ITU</MadCap:glossaryTerm> <MadCap:glossaryTerm>TCAP</MadCap:glossaryTerm>. In the following scenarios, the initial MSU is transmitted from SS7 node A to SS7 node B. A transaction block may only be opened for correlation by an ITU TCAP Begin MSU. Direction A to B is defined as forward and direction B to A is considered reverse in the <em>Filter Builder.</em></p>
        <h4><strong>Scenario 1:&#160;Basic 2-MSU – Completed transaction</strong>
        </h4>
        <p class="list_item">This example is the simplest completed transaction that can be monitored. The <span class="emph">TCAP Begin </span>is always stored in the first MSU position within the transaction block (01). The <span class="emph">TCAP End </span>is always stored in the last position within the transaction block (10). Because the Begin-End pair is complete, the disposition <span class="emph">TC1 </span>is assigned. </p>
        <div class="indent_nopagebreak">
            <p class="list_item">A&#160;&#160;B</p>
            <p class="linebreak">|→|&#160;(01)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP Begin-Invoke</p>
            <p class="linebreak">|←|&#160;(10)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP End-Return</p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Disposition = <strong>TC1</strong> </p>
        </div>
        <h4><strong>Scenario 2:&#160;Basic 4-MSU – Completed transaction</strong>
        </h4>
        <p class="list_item">This example is a completed transaction that contains <span class="emph">ITU TCAP Continue </span>pair(B) in addition to the Begin-End pair (A). Because Pair A and Pair B are both complete, the disposition <span class="emph">TC1</span> is assigned.</p>
        <div class="indent_nopagebreak">
            <p class="list_item">A&#160;&#160;B</p>
            <p class="linebreak">|→|&#160;(01)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP Begin-Invoke (Pair-A)</p>
            <p class="linebreak">|←|&#160;(02)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="blueitalic">TCAP Continue-Invoke(Pair-B)</span></p>
            <p class="linebreak">|→|&#160;(03)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="blueitalic">TCAP Continue-Return (Pair-B)</span></p>
            <p class="linebreak">|←|&#160;(10)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP End-Return (Pair-A)</p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Disposition = <strong>TC1</strong></p>
        </div>
        <h4><strong>Scenario 3:&#160;Full Transaction Block 10-MSU – Completed transaction</strong>
        </h4>
        <p class="list_item">This example is a completed transaction that fills all 10 available MSU positions in a transaction block. Up to four Continue pairs can be associated with the Begin-End pair (A) in the transaction block. Because pairs A, B, C, D, and E are all complete, the disposition <span class="emph">TC1 </span>is assigned.</p>
        <div class="indent_nopagebreak">
            <p class="list_item">A&#160;&#160;B</p>
            <p class="linebreak">|→|&#160;(01)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP Begin-Invoke pair-A</p>
            <p class="linebreak">|←|&#160;(02)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="blueitalic">TCAP Continue-Invoke pair-B</span></p>
            <p class="linebreak">|→|&#160;(03)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="blueitalic">TCAP Continue-Return pair-B</span></p>
            <p class="linebreak">|←|&#160;(04)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="purpleitalic">TCAP Continue-Invoke pair-C</span></p>
            <p class="linebreak">|→|&#160;(05)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="purpleitalic">TCAP Continue-Return pair-C</span></p>
            <p class="linebreak">|←|&#160;(06)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="greenitalic">TCAP Continue-Invoke pair-D</span></p>
            <p class="linebreak">|→|&#160;(07)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="greenitalic">TCAP Continue-Return pair-D</span></p>
            <p class="linebreak">|←|&#160;(08)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="orangeitalic">TCAP Continue-Invoke pair-E</span></p>
            <p class="linebreak">|→|&#160;(09)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="orangeitalic">TCAP Continue-Return pair-E</span></p>
            <p class="linebreak">|←|&#160;(10)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP End-Return pair-A</p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Disposition = <strong>TC1</strong></p>
        </div>
        <h4>Scenario 4:&#160;Missing End MSU– Incomplete transaction
        </h4>
        <p class="list_item">This example has an ITU TCAP Begin MSU opened with a transaction block. The End MSU was not found to have a matching correlation key before the correlation timer expired. The Begin MSU may be alone or there may be one or more Continue MSU pairs present and correlated. Because the last received MSU was not acknowledged in time, the disposition <span class="emph">TC2 </span>is assigned, indicating an incomplete transaction without a terminating End MSU. </p>
        <div class="indent_nopagebreak">
            <p class="list_item">A&#160;&#160;B</p>
            <p class="linebreak">|→|&#160;(01)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP Begin-Invoke pair-A</p>
            <p class="linebreak">|←|&#160;(02)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="blueitalic">TCAP Continue-Invoke pair-B (optional)</span></p>
            <p class="linebreak">|→|&#160;(03)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="blueitalic">TCAP Continue-Return pair-B (optional)</span></p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;:</p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;:</p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;– correlation timer expired</p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Disposition = <strong>TC2</strong></p>
        </div>
        <h4><strong>Scenario 5: End MSU without Open Transaction Block – Incomplete transaction</strong>
        </h4>
        <p class="list_item">In this example, an ITU TCAP End MSU has been received but a transaction block was not found open. The MSU cannot be correlated, so it is placed in a new transaction block and the disposition <span class="emph">TC3</span> is assigned, indicating an incomplete transaction consisting of a lone End MSU. </p>
        <div class="indent_nopagebreak">
            <p class="list_item">A&#160;&#160;B</p>
            <p class="linebreak">|←|&#160;(10)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP End-Return (with correlation key-X)</p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;–  no open transaction block for key-X</p>
            <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Disposition = <strong>TC3</strong></p>
        </div>
        <h4><strong>Scenario 6: Odd number of Continue MSUs – Incomplete transaction</strong>
        </h4>
        <p class="list_item">In this example, the Begin-End MSU pair-A has been monitored and correlated; however, there is an odd number of Continue MSUs correlated. The odd number of Continue MSUs indicates an incomplete Continue MSU pair, resulting in a disposition of TC4.</p>
        <p class="list_item">A&#160;&#160;B</p>
        <p class="linebreak">|→|&#160;(01)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP Begin-Invoke pair-A</p>
        <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="blueitalic">– missing MSU (Continue-Invoke pair-B)</span></p>
        <p class="linebreak">|→|&#160;(03)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<span class="blueitalic">TCAP Continue-Return pair-B</span></p>
        <p class="linebreak">|←|&#160;(10)&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TCAP End-Return pair-A</p>
        <p class="linebreak">|&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Disposition = <strong>TC4</strong></p>
    </body>
</html>