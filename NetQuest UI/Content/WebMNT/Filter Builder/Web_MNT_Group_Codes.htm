﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../../Resources/TableStyles/10pt-Basic.css" rel="stylesheet" MadCap:stylesheetType="table" />
        <link href="../../Resources/TableStyles/10pt-Basic.css" rel="stylesheet" MadCap:stylesheetType="table" />
        <link href="../../Resources/Stylesheets/BookStyles.css" rel="stylesheet" type="text/css" />
        <link href="../../Resources/Stylesheets/NetQuest_UI.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h2 class="pageBreak"><a name="Group"></a>
            <MadCap:keyword term="Group Codes" />Group Codes</h2>
        <h3 class="notoc">Introduction</h3>
        <p>Traditionally,  filters are defined to select specific information to examine, and discard the bulk of incoming traffic that are not relevant. In a correlated transaction, a filter would have to specify the field to examine, and also which <MadCap:glossaryTerm>MSU</MadCap:glossaryTerm> in the correlated block to search for the selected field. In protocols that have multiple MSUs in the correlated block, the process of setting up filters or an a-la-carte output to capture or print the exact occurrence of a field value could become cumbersome and use excessive filter resources.</p>
        <h3 class="notoc">Why Use Group Codes?</h3>
        <p>Group codes offer a simplified way to access values for either filtration or formatting for a few select common fields. To better understand the power of group field, here is a quick review of an <MadCap:glossaryTerm>ANSI</MadCap:glossaryTerm><MadCap:glossaryTerm>TCAP</MadCap:glossaryTerm> transaction. The typical transaction begins with a Begin (Q<em>uery</em>) package and ends with an End/Return <em>(Response)</em> package (see below). </p>
        <p><pre class="11pt_indent">|→    Begin        |
|←    End/Return   |</pre>
        </p>
        <p>Optionally, the transaction might contain <em>Conversation</em> packages between the Begin and End packages (see below). </p>
        <p><pre class="11pt_indent">|→    Begin        |
|←    Continue 1   |
|→    Continue 2   |
|←    End/Return   |</pre>
        </p>
        <p class="pageBreak">Further combinations in a transaction are possible. For example:</p>
        <table style="width: 100%;mc-table-style: url('../../Resources/TableStyles/10pt-Basic.css');" class="TableStyle-10pt-Basic" cellspacing="0">
            <col style="width: 156px;" class="TableStyle-10pt-Basic-Column-Column1" />
            <col class="TableStyle-10pt-Basic-Column-Column1" />
            <tbody>
                <tr class="TableStyle-10pt-Basic-Body-Body1">
                    <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Example 1</td>
                    <td class="TableStyle-10pt-Basic-BodyD-Column1-Body1">
                        <p><pre class="11pt_indent">|→    Begin        |
|←    Continue 1   |
|→    Continue 2   |
|←    Continue 3   |
|→       ...       |
|←       ...       |
|→    Continue 8   |
|←    End/Return   |</pre>
                        </p>
                    </td>
                </tr>
                <tr class="TableStyle-10pt-Basic-Body-Body1">
                    <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Example 2</td>
                    <td class="TableStyle-10pt-Basic-BodyD-Column1-Body1">
                        <p><pre class="11pt_indent">|→    Begin        |
|←    Abort        |</pre>
                        </p>
                    </td>
                </tr>
                <tr class="TableStyle-10pt-Basic-Body-Body1">
                    <td class="TableStyle-10pt-Basic-BodyB-Column1-Body1">Example 3</td>
                    <td class="TableStyle-10pt-Basic-BodyA-Column1-Body1">
                        <p><pre class="11pt_indent">|→    Notice       |</pre>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>Consolidating the fields makes it easier to create filters and helps prevent errors. Using fields, you can focus on the purpose of the filter instead of the housekeeping involved in multiple filters.</p>
        <h3 class="notoc">Filtration</h3>
        <p>For a conventional, non-group field,  a filter is normally launched for each MSU in the correlated transaction. For example, to find a field <span class="emph">A</span> with the value <span class="emph">X</span> in a transaction with four MSUs (1 – Begin, 2 x Continue, and 1 – End),  four filters are needed — one to locate the field with the desired value, plus one for each MSU. This is the only way to find field <span class="emph">A</span> if the MSU that carries it is unknown.</p>
        <p>A group field parameter simplifies the task, because  the filter is entered <span class="emph">only once</span>, for the complete transaction, regardless of the number of correlated MSUs. The filtration mechanism searches all MSUs sequentially, until parameter <span class="emph">A</span> is found with the value <span class="emph">X</span>. If found in any MSU, then the <MadCap:glossaryTerm>TDR</MadCap:glossaryTerm> is considered passed. Otherwise, if there is no match, the TDR is considered failed.</p>
        <p>Filtration defines the types of MSUs to be filtered. Build and name the new filter using the Filter Builder in the <MadCap:glossaryTerm>MNT</MadCap:glossaryTerm>, as follows:</p>
        <ol>
            <li class="step_listitem">Click the <span class="gui">Filtration</span> tab.</li>
            <li class="step_listitem">Define the fields to be filtered. For example, click <MadCap:glossaryTerm>ANSI-41</MadCap:glossaryTerm>, then select the field by double-clicking it. The Criteria Editor screen displays. </li>
            <li class="step_listitem">Select the criteria on which  to filter, then click <span class="gui">OK</span> to return to the <span class="gui">Filtration</span> tab. </li>
            <li class="step_listitem">Continue with any additional fields by double-clicking the field to see the Criteria Editor again. </li>
            <li class="step_listitem">When all relevant choices have been made, click <span class="gui">OK</span> to return to the <span class="gui">Filtration</span> tab.</li>
        </ol>
        <p class="keepwithnext">In the following figure, a group code is selected.</p>
        <p class="image">
            <img src="../../Resources/Images/Web_MNT_Group_Filter_Select.png" border="0" />
        </p>
        <p class="figure" MadCap:autonum="Figure 1: ">Group Code Selection Example</p>
        <h3 class="notoc">Formatting Output</h3>
        <p>For a conventional non-group field,  a field criteria selection needs to be made manually to print the values for each MSU in the related transaction. For example, to print a field <span class="emph">A</span> in a transaction with four MSUs, a begin, two continues and an end,  four field selections are needed to print each field, one for each MSU expected in the TDR. Typically, the fields would be separated by manually entered commas.</p>
        <p>A group field is a simplification because you enter it only once, for the complete transaction, regardless of the number of related MSUs. The formatting mechanism prints the value of <span class="emph">A</span> for each MSU sequentially. A comma is automatically used to separate the group field output.</p>
        <p>The following rules apply to group field output:</p>
        <ul>
            <li class="step_listitem">The first position is always the field value as contained in the TCAP Begin package. The second position is always the field value as contained in the TCAP End package. Each subsequent field represents the sequentially received field value contained in a TCAP Continue package.
            </li>
            <li class="step_listitem">Each group field value has a comma separating it from the next value.
            </li>
            <li class="step_listitem">If a field value is not present, nothing is printed except the comma. Commas are place holders needed to preserve the column positions for importing the output into a spreadsheet or database.
            </li>
        </ul>
        <p>The following example simulates printout from a capture program of selected output showing op code groups with other (italicized) fields:</p>
        <div class="Example_borderTopBottom_MarginTop">
            <p>&#160;<em>TC1,</em><span class="bluetext">RGN,0,0,0,,,,,,,</span><em>130.020.002</em></p>
            <p class="linebreak">&#160;<em>TC1,</em><span class="bluetext">RGN,0,0,0,,,,,,,</span><em>130.020.002</em></p>
            <p class="linebreak">&#160;<em>TC1,</em><span class="bluetext">RGN,0,0,0,,,,,,,</span><em>130.020.002</em></p>
        </div>
        <p>In this example, the fields <span class="bluetext">RGN,0,0,0,,,,,,,</span> are the <span class="emph">op codes</span>. The other fields are possible fields that might be included. The op code families show the value for each potential transaction — in this case, there are a query, response, and two conversations. The sample above is a very small sampling on a system with not much activity, and shows those MSUs that passed the filter.</p>
        <p class="note" MadCap:autonum="&lt;b&gt;Note: &#160;&lt;/b&gt;">Delimiting commas are always included — they are used as place-keepers to generate spreadsheets.</p>
        <p class="notepara2">Although the response (if there is one) is actually last in time as shown in the ladder diagram above, it is always stored in the second position within the group.</p>
        <p>On the <span class="gui">Output Format</span> tab of the Filter Builder, select what  to print from the ANSI-41 choices.</p>
        <p class="keepwithnext">For example, the disposition, a separating comma, and the op code. A <MadCap:glossaryTerm>CR/LF</MadCap:glossaryTerm> separates the individual transactions. </p>
        <p class="image">
            <img src="../../Resources/Images/Web_MNT_Output_Formate_DCO.png" border="0" alt="image\Group_Code_Output.png" class="listimage" />
        </p>
        <p class="figure" MadCap:autonum="Figure 2: ">Example Output Formats Section</p>
        <p MadCap:conditions="Global_Conditions.MNT-legacy">The results can be printed using any tools available. Right-click in the <span class="gui">Queue Status</span> quadrant of the MNT screen to see a pop-up menu listing the available choices, for example: telnet. </p>
        <h3>Available Group Fields</h3>
        <p>Group fields are currently available for the ANSI-41 protocol. In the ANSI-41 protocol, the five fields that can be used as group fields are:</p>
        <ul>
            <li class="bulletitem">Op code family</li>
            <li class="bulletitem">Op code specifier</li>
            <li class="bulletitem">Octet</li>
            <li class="bulletitem">Link</li>
            <li class="bulletitem">
                <MadCap:glossaryTerm>UTC</MadCap:glossaryTerm> record</li>
        </ul>
        <p class="keepwithnext">The following table shows the group codes available for the ANSI-41 protocol.</p>
        <table style="margin-left: auto;margin-right: auto;caption-side: bottom;width: 100%;mc-table-style: url('../../Resources/TableStyles/10pt-Basic.css');" class="TableStyle-10pt-Basic" cellspacing="0">
            <caption MadCap:autonum="Table 1: ">Group Codes for ANSI-41 </caption>
            <col class="TableStyle-10pt-Basic-Column-Column1" style="width: 73px;" />
            <col class="TableStyle-10pt-Basic-Column-Column1" />
            <col class="TableStyle-10pt-Basic-Column-Column1" style="width: 64px;" />
            <col style="width: 355px;" class="TableStyle-10pt-Basic-Column-Column1" />
            <thead>
                <tr class="TableStyle-10pt-Basic-Head-Header1">
                    <th class="TableStyle-10pt-Basic-HeadE-Column1-Header1">Field</th>
                    <th class="TableStyle-10pt-Basic-HeadE-Column1-Header1">Tekno Number</th>
                    <th class="TableStyle-10pt-Basic-HeadE-Column1-Header1">Source</th>
                    <th class="TableStyle-10pt-Basic-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tr class="TableStyle-10pt-Basic-Body-Body1">
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Op Code Family</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">4508</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">MSU</td>
                <td class="TableStyle-10pt-Basic-BodyD-Column1-Body1">Operation code; value is always 9</td>
            </tr>
            <tr class="TableStyle-10pt-Basic-Body-Body1">
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Op Code Specifier</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">4509</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">MSU</td>
                <td class="TableStyle-10pt-Basic-BodyD-Column1-Body1">Message type of MSU</td>
            </tr>
            <tr class="TableStyle-10pt-Basic-Body-Body1">
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Octet</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">4510</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Tekno</td>
                <td class="TableStyle-10pt-Basic-BodyD-Column1-Body1">Number of bytes in an MSU; length of MSU</td>
            </tr>
            <tr class="TableStyle-10pt-Basic-Body-Body1">
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Link</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">4511</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Tekno</td>
                <td class="TableStyle-10pt-Basic-BodyD-Column1-Body1">User-specified logical link on which MSU is found; from link configuration</td>
            </tr>
            <tr class="TableStyle-10pt-Basic-Body-Body1">
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">UTC</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">4512</td>
                <td class="TableStyle-10pt-Basic-BodyE-Column1-Body1">Tekno</td>
                <td class="TableStyle-10pt-Basic-BodyD-Column1-Body1">Universal time code; time stamp for MSU showing the time Tekno Telecom detected the MSU</td>
            </tr>
        </table>
        <p>For example, if<span class="emph">UTC records</span> is chosen,  the UTC of the query, the response, and up to eight conversations displays. If  <span class="emph">octets</span> is chosen,  the octet count of the query, the octet count of the response, and the octet counts of up to eight conversations displays. All MSUs in the call block are affected — if any MSU in the call block meets the specified group requirement, the entire MSU passes the filter.</p>
        <!-- <p MadCap:conditions="Global_Conditions.ScreenOnly">
                <MadCap:relatedTopics>
                    <MadCap:relatedTopic src="Web_MNT_Data_Collection.htm#Data" />
                    <MadCap:relatedTopic src="Filtration/Web_MNT_Filtration.htm#top" />
                    <MadCap:relatedTopic src="Output_Format/Web_MNT_Output_Format.htm#top" />
                </MadCap:relatedTopics>
            </p> -->
    </body>
</html>