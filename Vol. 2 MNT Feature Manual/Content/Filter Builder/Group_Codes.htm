﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="7" MadCap:lastHeight="649" MadCap:lastWidth="576">
    <head><title>Group Codes</title>
        <script language="JavaScript" title="BSSC Special Effects" src="../ehlpdhtm.js">
        </script>
        <link href="../Resources/Stylesheets/BookStyles.css" rel="stylesheet" type="text/css" />
        <link href="../Resources/TableStyles/Basic.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h2 class="pageBreak">
            <MadCap:keyword term="Group Codes" />Group Codes</h2>
        <h3>Introduction</h3>
        <p>Typically,  filters are defined to select specific information to examine, and discard the bulk of incoming traffic that are not relevant. In a correlated transaction, a filter must specify not only the field to examine, but also which <MadCap:glossaryTerm glossTerm="TeknoTelecomGlossary.Term130">MSU</MadCap:glossaryTerm>s in the correlated block to search for the selected field. For protocols that have multiple MSUs in the correlated block, the process of setting up filters or an à-la-carte output to capture the exact occurrence of a field value can be cumbersome and consume system resources. <span class="emph">Group codes</span> offer a simplified way to access values for either filtration or formatting on select, common fields. </p>
        <h3>Why Use Group Codes?</h3>
        <p>To help illustrate the power of grouping fields, this topic provides an overview of an <MadCap:glossaryTerm glossTerm="TeknoTelecomGlossary.Term24">ANSI</MadCap:glossaryTerm><MadCap:glossaryTerm glossTerm="TeknoTelecomGlossary.Term188">TCAP</MadCap:glossaryTerm> transaction, as an example. </p>
        <p>The typical transaction begins with a Begin (Q<em>uery</em>) package and ends with an End/Return <em>(Response)</em> package (see below). </p>
        <p><pre class="11pt_indent">|→    Begin        |
|←    End/Return   |</pre>
        </p>
        <p>Optionally, the transaction may also contain <em>Conversation</em> packages between the Begin and End packages. </p>
        <p><pre class="11pt_indent">|→    Begin        |
|←    Continue 1   |
|→    Continue 2   |
|←    End/Return   |</pre>
        </p>
        <p class="pageBreak">Other combinations in a transaction are possible. They can be represented in the transaction as any of the following:</p>
        <table style="width: 100%;">
            <col />
            <col />
            <tbody>
                <tr>
                    <td>Example 1:</td>
                    <td>
                        <p><pre class="11pt_indent">|→    Begin        |
|←    Continue 1   |
|→    Continue 2   |
|←    Continue 3   |
|→       ...       |
|←       ...       |
|→    Continue 8   |
|←    End/Return   |</pre>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>Example 2:</td>
                    <td>
                        <p><pre class="11pt_indent">|→    Begin        |
|←    Abort        |</pre>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>Example 3:</td>
                    <td>
                        <p><pre class="11pt_indent">|→    Notice       |</pre>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>Consolidating fields by type makes it easier to create filters and to avoid errors. Using fields, you can focus on the purpose of the filter and less about the housekeeping involved in maintaining multiple filters.</p>
        <h3>Filtration</h3>
        <p>For a conventional, non-group field,  a filter is normally launched for each MSU in the correlated transaction. For example, to find a field <span class="emph">A</span> with the value <span class="emph">X</span> in a transaction with four MSUs (Begin, Continue, Continue, End),  four filters are needed to locate the field with the desired value, one for each MSU. This is the only way to find field A if the MSU that carries it is unknown.</p>
        <p>A group field parameter simplifies the task, because  <span class="emph">the filter is entered only </span><strong><span class="emph" style="font-weight: normal;">once</span>,</strong> for the complete transaction, independent of the number of correlated MSUs. The filtration mechanism searches all MSUs sequentially, until field A is found with the value X. If found in any MSU, then the <MadCap:glossaryTerm glossTerm="TeknoTelecomGlossary.Term192">xDR</MadCap:glossaryTerm> is considered passed. If there is no match, the xDR is considered failed.</p>
        <p class="keepwithnext"><span class="emph">Filtration</span> defines the attributes of MSUs to be filtered. You build a filter using the Filter Builder in the <MadCap:glossaryTerm glossTerm="TeknoTelecomGlossary.Term126">MNT</MadCap:glossaryTerm>. (For more information, see <MadCap:xref href="Filter_Builder_Overview.htm"><b>Filter Builder Overview</b> on page <b>1</b>.) </MadCap:xref>In the following figure, a group code is selected.</p>
        <p>
            <img src="../Resources/Images/Group_Code_Filtration.png" border="0" height="567" width="776" alt="image\Group_Code_Filtration.png" />
        </p>
        <p class="figure" MadCap:autonum="Figure 1: ">Filter Buidler – Group Code Selected</p>
        <h3>Formatting Output</h3>
        <p>For a conventional, non-group field,  a field criteria selection must be made manually to print the values for each MSU in the related transaction. For example, to print a field A in a transaction consisting of four MSUs, (begin, two continues and an end),  four field selections are needed — one for each MSU expected in the xDR. Typically, the fields would be separated by manually entering commas.</p>
        <p>A group field is simpler because you enter it only once, for the complete transaction, independent of the number of related MSUs. The formatting mechanism prints the value of A for each MSU sequentially. A comma is automatically used to separate the group field output.</p>
        <p class="keepwithnext">The following rules apply to group field output:</p>
        <ul>
            <li class="step_listitem">The first position is always the field value as contained in the TCAP Begin package. The second position is always the field value as contained in the TCAP End package. Each subsequent field represents the sequentially received field value contained in a TCAP Continue package.
            </li>
            <li class="step_listitem">Each group field value has a comma separating it from the next value.
            </li>
            <li class="step_listitem">If a field value is not present, nothing is printed except the comma. Commas are place holders needed to preserve the column positions for importing the output into a spreadsheet or database.
            </li>
        </ul>
        <p>The following example is a printout from a capture program of selected output showing op code groups with other (italicized) fields:</p>
        <div class="ExampleCode"><pre><em>TC1,</em><span class="bluetext">RGN,0,0,0,,,,,,,</span><em>130.020.002</em></pre><pre><em>TC1,</em><span class="bluetext">RGN,0,0,0,,,,,,,</span><em>130.020.002</em></pre><pre><em>TC1,</em><span class="bluetext">RGN,0,0,0,,,,,,,</span><em>130.020.002</em></pre>
        </div>
        <p>In this example, the fields <span class="bluetext">RGN,0,0,0,,,,,,,</span> represent the op codes. The other fields are possible fields that might be included. The op code families show the value for each potential transaction — in this case, a query, response, and two conversations. </p>
        <p>The sample above is a small sampling on a system with little activity, and shows those MSUs that passed the filter.   Delimiting commas are always included; they are used as placeholders for generating spreadsheets.
            </p>
        <p class="note" MadCap:autonum="&lt;b&gt;Note: &#160;&lt;/b&gt;">Although the Response (if there is one) is actually <span class="emph">last</span> in the sequence (as shown in the example), it is always stored in the <span class="emph">second</span> position within the group.</p>
        <p>Go to the Output Format screen of the Filter Builder in the MNT. From the ANSI-41 choices, select what  to print. For example: the <span class="emph">disposition</span>, one <span class="emph">separating comma</span>, and the <span class="emph">op code</span>. A <MadCap:glossaryTerm glossTerm="TeknoTelecomGlossary.Term58">CR/LF</MadCap:glossaryTerm> separates the individual transactions. </p>
        <p class="keepwithnext">The following figure shows an example of the output format screen:</p>
        <p class="image">
            <img src="../Resources/Images/Group_Code_Output.png" border="0" height="567" width="776" alt="image\Group_Code_Output.png" />
        </p>
        <p class="figure" MadCap:autonum="Figure 2: ">Example Output Format Screen</p>
        <p MadCap:conditions="Global_Conditions.MNT-legacy">The results can be printed using any tools available. Right-click in the Queue Status quadrant of the MNT screen to see a pop-up menu listing the available choices, for example: telnet. See <MadCap:xref href="../Installation_Configuration/DataCollection.htm" target="" title="" alt=""><b>"Data Collection"</b> on page <b>1</b></MadCap:xref> for more information on using telnet.</p>
        <h3>Currently Available Group Fields</h3>
        <p>Group fields are currently available for the ANSI-41 protocol. In the ANSI-41 protocol, the five fields that can be used as group fields are:</p>
        <ul>
            <li class="bulletitem">Op code family</li>
            <li class="bulletitem">Op code specifier</li>
            <li class="bulletitem">Octet</li>
            <li class="bulletitem">Link</li>
            <li class="bulletitem">
                <MadCap:glossaryTerm glossTerm="TeknoTelecomGlossary.Term8">UTC</MadCap:glossaryTerm> record</li>
        </ul>
        <div class="nopagebreak-inside">
            <p class="keepwithnext">The following table shows the group codes available for the ANSI-41 protocol.</p>
            <table style="mc-table-style: url('../Resources/TableStyles/Basic.css');margin-left: auto;margin-right: auto;caption-side: bottom;width: 100%;" class="TableStyle-Basic" cellspacing="0">
                <caption MadCap:autonum="Table 1: ">Available Group Codes for ANSI-41</caption>
                <col class="TableStyle-Basic-Column-Column1" />
                <col class="TableStyle-Basic-Column-Column1" />
                <col class="TableStyle-Basic-Column-Column1" />
                <col style="width: 355px;" class="TableStyle-Basic-Column-Column1" />
                <thead>
                    <tr class="TableStyle-Basic-Head-Header1">
                        <th class="TableStyle-Basic-HeadE-Column1-Header1">Field</th>
                        <th class="TableStyle-Basic-HeadE-Column1-Header1">Tekno Number</th>
                        <th class="TableStyle-Basic-HeadE-Column1-Header1">Source</th>
                        <th class="TableStyle-Basic-HeadD-Column1-Header1">Description</th>
                    </tr>
                </thead>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">Op Code Family</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">4508</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">MSU</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1">Operation code — value is always 9</td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">Op Code Specifier</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">4509</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">MSU</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1">Message type of MSU</td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">Octet</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">4510</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">Tekno</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1">Number of bytes in an MSU (length of MSU)</td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">Link</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">4511</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">Tekno</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1">User-specified logical Link on which MSU is found — read from <MadCap:snippetText src="../Resources/Snippets/LinkConfigTable.flsnp" /><p class="Example_borderTopBottom_MarginTop">For more information about administering the Link Table, refer to the <span class="italic">NetAnalyzer Vol. 3 – Link Table Administration Manual</span> (Doc. #T10091-803-01).</p></td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">UTC</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">4512</td>
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">Tekno</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1">UTC time stamp for MSU, showing the time that the <MadCap:snippetText src="../Resources/Snippets/NetAnalyzer.flsnp" /> detected the MSU</td>
                </tr>
            </table>
            <p>For example, if  <span class="emph">UTC records</span> is chosen,  the UTC of the query, the response, and up to eight conversations displays. If  <span class="emph">octets</span> is chosen,  the octet count of the query, the octet count of the response, and the octet counts of up to eight conversations displays. </p>
            <p class="note" MadCap:autonum="&lt;b&gt;Note: &#160;&lt;/b&gt;">All MSUs in the call block are affected. If any MSU in the call block meets the specified group criteria, the entire MSU passes the filter.</p>
            <p MadCap:conditions="Global_Conditions.ScreenOnly">
                <MadCap:relatedTopics>
                    <MadCap:relatedTopic src="../Installation_Configuration/DataCollection.htm#Data" />
                    <MadCap:relatedTopic src="Filtration/Filtration.htm" />
                    <MadCap:relatedTopic src="Output_Format/Output_Format.htm" />
                </MadCap:relatedTopics>
            </p>
        </div>
    </body>
</html>