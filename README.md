# README #

### What is this repository for? ###

Master repository for all documentation source, print and online.

### Contribution guidelines ###

Don't. 

Publishable artefacts are _built_ from this source code. Online help that must be integrated into an application is generated and then committed to the appropriate product-code repository.

### Who do I talk to? ###

* Repo owner: Pete Uremovic
* Other community or team contact: Don Malesh