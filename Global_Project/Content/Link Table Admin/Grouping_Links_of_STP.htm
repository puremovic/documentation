﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us" MadCap:ignoredWords="4xxx;xxx;N;2xxx;3xxx;csv;0xxx" MadCap:lastBlockDepth="6" MadCap:lastHeight="830" MadCap:lastWidth="576">
    <head>
        <link href="../Resources/TableStyles/Basic.css" rel="stylesheet" MadCap:stylesheetType="table" /><title>Grouping Links of an STP</title>
        <link href="../Resources/Stylesheets/BookStyles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h2 class="pageBreak">Grouping Links of an STP</h2>
        <h3><a name="Background_Information"></a>Background Information</h3>
        <p>A Signaling Transfer Point (STP) does not originate a call or a query transaction, but rather routes a call’s MSUs and transactions from other STPs or Signaling Point (SP) nodes to the call’s destination node. An STP is normally mated with a redundant STP to load share data and provide failover capability in case one of its Linksets fail or the STP itself fails to function.</p>
        <p>There are generally four types of signaling links resident on an STP:</p>
        <ul>
            <li class="bulletitem">“A” links transfer MSUs between the STP and a Signaling Point (SP) node or a Service Control Point (SCP) database.</li>
            <li class="bulletitem">"B" links transfer MSUs between mated STPs within the same quad configuration.</li>
            <li class="bulletitem">"C" links transfer MSUs between mated STPs.</li>
            <li class="bulletitem">"D" links transfer MSUs between mated STPs and mated STPs of other networks.</li>
        </ul>
        <p>The <MadCap:snippetText src="../Resources/Snippets/NetAnalyzer.flsnp" /> link table provides the following functionality:</p>
        <ul>
            <li class="bulletitem">It defines the link characteristics to decode and correlate MSUs transferred on the link.</li>
            <li class="bulletitem">It identifies the links that share the same nodes and can be grouped into a Linkset.</li>
            <li class="bulletitem">It identifies the Linksets that provide alternate routes to or from a node and can be grouped into a Combined Linkset.</li>
            <li class="bulletitem">It identifies associated Linksets and Combined Linksets on mated STPs servicing the same remote nodes.</li>
            <li class="bulletitem">It identifies associated Linksets and Combined Linksets on mated STPs as a single virtual Correlate Set for the purpose of correlating MSUs. Each of the mated STPs services one half of a Correlate Set.</li>
            <li class="bulletitem">It assigns STP feeds to a Branch Server and a Branch Manager to decode MSUs.</li>
            <li class="bulletitem">It assigns virtual Correlate Sets to a mating Branch Server and Branch Controller to correlate MSUs.</li>
        </ul>
        <p>A CLLI Code is a physical area location identifier. Multiple STPs can be identified by the same CLLI location.</p>
        <p>A Point Code is a Signaling Point’s unique address. Its format is three numeric segments usually separated by periods or hyphens. The Point Code segments are named network.cluster.member or network.region.switch. For the ANSI routing protocol the segments range from 0 to 255. For the ITU routing protocol the network and member segments range from 0 to 7 and the cluster segment ranges from 0 to 255. An STP Point Code may be identified by having a low network number in the range 0 to 5 or by having a zero member number.</p>
        <p class="note"><strong>Note:</strong> This may not always be true for smaller carriers.</p>
        <p>A network configuration of STPs, SPs, SCPs and <MadCap:snippetText src="../Resources/Snippets/NetAnalyzer.flsnp" />s is described below.</p>
        <p class="image">
            <img src="../Resources/Images/T261B_Typical_System_Configuration-Generic.png" />
        </p>
        <p class="figure" MadCap:autonum="Figure 1: "><a name="Sample_Network_Configuration"></a> Sample Network Configuration</p>
        <p>Depending upon the point codes, some “D” links are treated as “B” links for the purpose of creating a link table to correlate MSUs between mated <MadCap:snippetText src="../Resources/Snippets/NetAnalyzer.flsnp" />s. This example describes the development of a link table for STP A which has all four types of links.</p>
        <h3>Link Grouping and Designation Procedure</h3>
        <p>The following sections describe the creation of a link table for STP A in a network described by the <MadCap:xref href="#Sample_Network_Configuration"><b> Sample Network Configuration</b> on page <b>1</b></MadCap:xref></p>
        <ol>
            <li class="step_listitem">Sort the links originating on STP A by their remote node’s Destination CLLI Code and Point Code,</li>
            <li class="step_listitem">Identify STP A’s “C” links. They have the mated STP B Point Code.</li>
            <li class="step_listitem">Identify STP A’s “B” links. They have the Point Codes of the other two STPs in the quad configuration, STP S and STP T.</li>
            <li class="step_listitem">Identify STP A’s “D” links. They have the Point Codes of STPs not in the quad configuration, STP G and STP H.</li>
            <li class="step_listitem">Identify the remaining links as STP A’s “A” links.</li>
            <li class="step_listitem">Assign Linkset names to the links with the same Destination Point Code. Tekno recommends the Telcordia Two Six Code (TSC) standard.</li>
            <li class="step_listitem">Group the “B” link Linksets from STP A to the remote STP S and STP T nodes in the quad configuration. Assign Combined Linkset identifiers to the groups.</li>
            <li class="step_listitem">Group the “D” link Linksets from STP A to the external STP G and STP H nodes. Assign Combined Linkset identifiers to the groups.</li>
            <li class="step_listitem">Assign Correlate Set numbers to the links in the “A” link Linksets, the “B” link Combined Linksets, the “C” link Linkset and the “D” link Combined Linksets. The same Correlate Set numbers are assigned to the alternate route Linksets and Combined Linksets on STP B. Together they form the MSU correlation route. Assign each link a Low Speed Link Equivalent value using the transaction rate provided by the customer or these weighted Low Speed Link Equivalents by Circuit Type.</li>
        </ol>
        <table style="caption-side: top; mc-table-style: url('../Resources/TableStyles/Basic.css'); margin-left: auto; margin-right: auto;" class="TableStyle-Basic" cellspacing="0">
            <col class="TableStyle-Basic-Column-Column1" />
            <col class="TableStyle-Basic-Column-Column1" />
            <thead>
                <tr class="TableStyle-Basic-Head-Header1">
                    <th class="TableStyle-Basic-HeadD-Column1-Header1" colspan="2">Low Speed Link Equivalents</th>
                </tr>
                <tr class="TableStyle-Basic-Head-Header1">
                    <th class="TableStyle-Basic-HeadE-Column1-Header1">Circuit Type</th>
                    <th class="TableStyle-Basic-HeadD-Column1-Header1">Low Speed <br />Link Equivalent</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">ATM</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1" style="text-align: right;">8</td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">E1</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1" style="text-align: right;">30</td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">T1</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1" style="text-align: right;">24</td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">IP</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1" style="text-align: right;">1</td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyE-Column1-Body1">IP Failover</td>
                    <td class="TableStyle-Basic-BodyD-Column1-Body1" style="text-align: right;">0</td>
                </tr>
                <tr class="TableStyle-Basic-Body-Body1">
                    <td class="TableStyle-Basic-BodyB-Column1-Body1">Redundant IP</td>
                    <td class="TableStyle-Basic-BodyA-Column1-Body1" style="text-align: right;">0</td>
                </tr>
            </tbody>
        </table>
        <h3><a name="Correlate_Set_Number_for_Single_STP_Networks"></a>Correlate Set Numbers for Single STP Networks</h3>
        <p>An IP Link is considered an “A” link. Its Correlate Set number is prefixed by a zero and the Point Code number of its SP node with the segment separators removed.</p>
        <p>An “A” link Correlate Set number is prefixed by a one and the lowest Point Code number of its SP node with the segment separators removed.</p>
        <p>A “B” link Correlate Set number is prefixed by a two and the Network and Cluster numbers, lowest first, of the distant STP nodes.</p>
        <p>A “C” link Correlate Set number is prefixed by a three and the lowest Point Code number of the STP nodes with the segment separators removed.</p>
        <p>A “D” link Correlate Set number is prefixed by a four and the lowest full Point Code number of the STP nodes with the segment separators removed.</p>
        <div>
            <h3><a name="Correlate_Set_Designations_for_Multiple_STP_Networks"></a>Correlate Set Designations for Multiple STP Networks</h3>
            <p>Using the Destination Point Code (DPC) as the basis for building a unique Correlate Set number is appropriate for a single STP pair network. However, when a network is composed of many STP pairs that address the same DPCs, the following concept is used.</p>
            <p>The Correlate Set number is still ten digits in length. The most significant digit does not exceed two. The first six digits would be the Region Codes of the mated STP’s Originating Point Code (OPC). The master STP Region Code is followed by the slave STP Region Code. As an example:</p>
            <p class="Example_TopBorder">STP A OPC = 001.193.000</p>
            <p class="Example_BottomBorder">STP B OPC = 001.194.000</p>
            <p>The first six digits of the Correlate Set number is:</p>
            <p class="Example_borderTopBottom_MarginTop">193194</p>
            <p>If the Region Code is one or two digits, precede the digits with zeros.</p>
            <p class="Example_TopBorder">STP S OPC = 001.066.000</p>
            <p class="Example_BottomBorder">STP T OPC = 001.034.000</p>
            <p>The first six digits of the Correlate Set number is</p>
            <p class="Example_borderTopBottom_MarginTop">066034</p>
            <p>The remaining four digits of the Correlate Set number represent its identity within the network. One thousand numbers are allocated to each STP pair, numbered 0 to 999.</p>
            <table style="margin-left: auto;margin-right: auto;caption-side: top;mc-table-style: url('../Resources/TableStyles/Basic.css');" class="TableStyle-Basic" cellspacing="0">
                <col class="TableStyle-Basic-Column-Column1" />
                <col class="TableStyle-Basic-Column-Column1" />
                <col class="TableStyle-Basic-Column-Column1" />
                <thead>
                    <tr class="TableStyle-Basic-Head-Header1">
                        <th class="TableStyle-Basic-HeadE-Column1-Header1" colspan="2">Site</th>
                        <th class="TableStyle-Basic-HeadD-Column1-Header1">Range</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP A</td>
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP B</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">0000 - 0999</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP S</td>
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP T</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">1000 - 1999</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP Q</td>
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP R</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">2000 - 2999</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP M</td>
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP N</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">3000 - 3999</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">International STP K</td>
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">International STP L</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">4000 - 4999</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP I</td>
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">STP J</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">5000 - 5999</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyB-Column1-Body1">STP G</td>
                        <td class="TableStyle-Basic-BodyB-Column1-Body1">STP H</td>
                        <td class="TableStyle-Basic-BodyA-Column1-Body1">6000 - 6999</td>
                    </tr>
                </tbody>
            </table>
            <p>Once again fill in the leading zeros so that the Correlate Set’s Region Code is evident. The first Correlate Set number for the STP A / STP B pair is</p>
            <p class="Example_borderTopBottom_MarginTop">1931940000</p>
            <p>The first Correlate Set number for the STP S / STP T pair is</p>
            <p class="Example_borderTopBottom_MarginTop">0660341000</p>
            <p>The thousand numbers allocated to a STP pair are subdivided by Link Type.</p>
            <table style="mc-table-style: url('../Resources/TableStyles/Basic.css');margin-left: auto;margin-right: auto;caption-side: top;" class="TableStyle-Basic" cellspacing="0">
                <col class="TableStyle-Basic-Column-Column1" />
                <col class="TableStyle-Basic-Column-Column1" />
                <thead>
                    <tr class="TableStyle-Basic-Head-Header1">
                        <th class="TableStyle-Basic-HeadD-Column1-Header1" colspan="2">STP A and STP B</th>
                    </tr>
                    <tr class="TableStyle-Basic-Head-Header1">
                        <th class="TableStyle-Basic-HeadE-Column1-Header1">Link Type</th>
                        <th class="TableStyle-Basic-HeadD-Column1-Header1">Range</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">IP</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">0000 - 0199</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">A</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">0200  - 0699</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">B</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">0700 - 0799</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">D</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">0800 - 0899</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyB-Column1-Body1">Others</td>
                        <td class="TableStyle-Basic-BodyA-Column1-Body1">0900 - 0999</td>
                    </tr>
                </tbody>
            </table>
            <p>&#160;</p>
            <table style="mc-table-style: url('../Resources/TableStyles/Basic.css');margin-left: auto;margin-right: auto;caption-side: top;" class="TableStyle-Basic" cellspacing="0">
                <col class="TableStyle-Basic-Column-Column1" />
                <col class="TableStyle-Basic-Column-Column1" />
                <thead>
                    <tr class="TableStyle-Basic-Head-Header1">
                        <th class="TableStyle-Basic-HeadD-Column1-Header1" colspan="2">STP S and STP T</th>
                    </tr>
                    <tr class="TableStyle-Basic-Head-Header1">
                        <th class="TableStyle-Basic-HeadE-Column1-Header1">Link Type</th>
                        <th class="TableStyle-Basic-HeadD-Column1-Header1">Range</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">IP</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">1000 - 1199</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">A</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">1200 - 1699</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">B</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">1700 - 1799</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyE-Column1-Body1">D</td>
                        <td class="TableStyle-Basic-BodyD-Column1-Body1">1800 - 1899</td>
                    </tr>
                    <tr class="TableStyle-Basic-Body-Body1">
                        <td class="TableStyle-Basic-BodyB-Column1-Body1">Others</td>
                        <td class="TableStyle-Basic-BodyA-Column1-Body1">1900 - 1999</td>
                    </tr>
                </tbody>
            </table>
            <p>The Correlate Set number 1931940200 is interpreted as:</p>
            <p class="Example_TopBorder">193 - STP A Region Code</p>
            <p class="Example_noborder">194 - STP B Region Code</p>
            <p class="Example_BottomBorder">200 - Link Type “A” Correlate Set 200</p>
            <p>This numbering concept provides maximum interpretation without duplication.</p>
        </div>
        <h3 class="h3sb" MadCap:conditions="Global_Conditions.Hidden"><strong>Link Table Assignments for the</strong> <MadCap:snippetText src="../Resources/Snippets/NetAnalyzer 3000.flsnp" style="font-weight: normal;" /></h3>
        <p MadCap:conditions="Global_Conditions.Hidden">Now that the links on the STP have been appropriately grouped into half linksets and each half linkset is assigned a low speed link equivalent, proceed to assign the links appearing on the ILM feeds to a Branch and Branch Controller of the <MadCap:snippetText src="../Resources/Snippets/NetAnalyzer.flsnp" /> for balanced loading and accurate correlation of MSUs respectively:</p>
        <ol>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">Determine the number of low speed link equivalents "physically" connected to each Branch for each half linkset.</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">Sort the half linksets by size (total number of low speed link equivalents) with largest first.</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">Assign the first (largest) half linkset to the Branch with the greatest number of physically connected low speed links associated with that linkset.</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">Assign the second half linkset etc. in a similar manner until all half linksets are assigned to a Branch for correlation.</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">Do not assign more than 1200 low speed link equivalents in total to a Branch for correlation, if that amount is reached then assign any remaining half linksets with the greatest number of links on that Branch to the Branch with the second greatest number of links from the same half linkset.</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">If two or more Branches have an equal number of physically connected links associated with the same half linkset, assign the half linkset to the Branch with the least number of total low speed link equivalents assigned to it.</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">Once all half linksets are assigned to a Branch, assign each half linkset in the same chronological order to a Branch Controller within its assigned Branch for correlation. (Do not exceed 400 low speed link equivalent assignments to a single Branch Controller).</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">When completed, shift half linkset assignments within the Branch so the Branch Controller loading is somewhat balanced.</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">Generate link table for the <MadCap:snippetText src="../Resources/Snippets/NetAnalyzer 3000.flsnp" /> based on resultant configuration.</li>
            <li class="step_listitem" MadCap:conditions="Global_Conditions.Hidden">Generate a link table for the mating <MadCap:snippetText src="../Resources/Snippets/NetAnalyzer 3000.flsnp" /> as a mirror image to the above table to assure correlation of MSUs into CDRs, TDRs and NDRs.</li>
        </ol>
    </body>
</html>